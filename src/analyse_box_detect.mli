(* analyse_box_detect.mli *)

(* val analyse_box_detect : Sdlvideo.surface -> (int * int * int * int) list *)
val horizontal_box_detect : Sdlvideo.surface -> (int * int * int * int) list
val vertical_box_detect : Sdlvideo.surface ->
  (int * int * int * int) list ->
  (int * int * int * int) list
