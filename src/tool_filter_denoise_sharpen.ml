(* tool_filter_denoise_sharpen.ml *)

let _ =
  Helpers.main_filter_copy
    "Sharpen image"
    Filter_denoise_sharpen.filter_denoise_sharpen
