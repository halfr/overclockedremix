(* filter_denoise_box_blur.ml *)

let filter_denoise_box_blur surface =
  let box_blur_kernel = [|
    [|  1.;  1.;  1. |];
    [|  1.;  1.;  1. |];
    [|  1.;  1.;  1. |];
  |] in
    Lib_convolution.convolve_value
      ~src:surface
      ~kernel:box_blur_kernel
      ~divisor:9.0
      ~offset:0.0;
