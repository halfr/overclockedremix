(* filter_unskew.ml *)

let square x = x * x

let pi = 3.14159265358979323846

let deg_to_rad deg =
  ((pi *. deg) /. (180.))

(* The original matrix is:   and the new one will be:*)
(* |x| 											|x'| *)
(* |y| 											|y'| *)
(* x' = x*cosangle - y*singangle and y' = x*sinangle + y*cosangle   *)

let new_x i j center_x center_y angle =
  center_x + truncate( float_of_int(i-center_x)*.(cos angle) -. float_of_int(j-center_y)*.(sin angle))

let new_y i j center_x center_y angle =
  center_y + truncate( float_of_int(i-center_x)*.(sin angle) +. float_of_int(j-center_y)*.(cos angle))

(*Calcule le nombre de pixels noirs dans l'image *)
let blackPixelsNumber img =
  let k = ref 0 in
  let (w,h) = Helpers.surface_size img in
    for i = 0 to h - 1 do
      for j = 0 to w - 1 do
        if (Sdlvideo.get_pixel_color img i j) = (0,0,0) then
          k := (!k) + 1
      done;
    done;
    !k

(* Enregistre les coord (x,y) des pixels noirs dans l'image et les renvoi dans un tableau*)
let list_blackPixels_coords bw_img =
  let (w,h) = Helpers.surface_size bw_img in
  let list_coords = ref [] in
    for i = 0 to h - 1 do
      for j = 0 to w - 1 do
        if Sdlvideo.get_pixel_color bw_img i j = (0,0,0) then
          list_coords := (i,j)::(!list_coords)
      done;
    done;
    (Array.of_list (!list_coords))

(* create_R_alpha_matrix: Cr�� une fonction qui va cr�er
 la matrice R - alpha, avec r le num�ro de la ligne, et alpha la colonne
 d'o� une initialisation � alpha=180 pour la 1er matrice, 
 et r = grande valeur au cas o� (les -1 nous stoppent d�s que besoin pour la prochaine fonction)*)
(* r_value: Calcul de r  (distance de rot)  pour un pixel (x,y)
 et pour un angle alpha *)

let create_R_alpha_matrix bw_img =
  let list_coords = list_blackPixels_coords bw_img in
  let nbpxbk = blackPixelsNumber bw_img in
  let r_values = ref 0 in

  let r_value x y alpha =
    let alpha = deg_to_rad (float_of_int(alpha)) in
      truncate(abs_float(float_of_int(x) *. cos(alpha)
                         +. float_of_int(y) *. sin(alpha))) in	

  let matrix_r_alpha = Array.create_matrix 2000 2000 (-1) in
    for alpha = 0 to 180 do
      for i = 0 to (nbpxbk - 1)  do
        let (x,y) = list_coords.(i) in
          r_values := r_value x y alpha;
          matrix_r_alpha.(!r_values).(alpha) <- matrix_r_alpha.(!r_values).(alpha) + 1;
      done;
    done;
    matrix_r_alpha

let getAngle bw_img =
  let (w,h) = Helpers.surface_size bw_img in
  let matrix_r_alpha= create_R_alpha_matrix bw_img in (*on cr�� la matrice des R et alpha depuis l'image noir et blanc*)
  let i = ref 1 in
  let accu_r1 = ref 0 and accu_r2 = ref 0 in (* on cr�� les deux accumulateurs (voir algo de Hough)*)
  let alpha_max = ref (-1) in
    for alpha = 0 to 180 do
      while (!i < h - 1) && (matrix_r_alpha.(!i + 1).(alpha) <> (-1)) do (*parcours des lignes pour r, si -1, case non compl�t�e precedemment*)
        accu_r1 := !accu_r1 + square ((matrix_r_alpha.(!i).(alpha)) - (matrix_r_alpha.(!i - 1).(alpha)));
        i := !i + 1;
      done;
      accu_r1 :=  !accu_r1 + square ((matrix_r_alpha.(!i).(alpha)) - (matrix_r_alpha.(!i - 1).(alpha)));
      if (!accu_r1 > !accu_r2)  then (* si l'accu1 > accu2, alors on a un nouvel angle max trouv�*)
        begin
          accu_r2 := !accu_r1;
          alpha_max := alpha;
        end;
      accu_r1 := 0;
    done;
    !alpha_max

(* rotation d'une matrice noir et blanche, merci de passer l'angle en radian avant de l'utiliser... *)

let rotate img angle =
  Sdlvideo.save_BMP img "/tmp/tmp.bmp";
  ignore(Sys.command "mogrify -deskew 40 /tmp/tmp.bmp");
  Sdlloader.load_image "/tmp/tmp.bmp"

let filter_unskew img =
  rotate img 42
