(* filter_grayscale.ml *)
(* Author: Rémi Audebert *)

(* https://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale *)
let luminance (r, g, b) =
  int_of_float (0.299 *. (float r) +. 0.587 *. (float g) +. 0.114 *. (float b))

let scalar_to_triplet v =
  (v, v, v)

let pixel_rgb_to_grayscale surface (x, y) =
  let pixel = Sdlvideo.get_pixel_color surface x y in
  let new_pixel = scalar_to_triplet (luminance pixel) in
    Sdlvideo.put_pixel_color surface x y new_pixel

(* apply luminance filter *)
let filter_grayscale surface =
  Helpers.surface_iter pixel_rgb_to_grayscale surface
