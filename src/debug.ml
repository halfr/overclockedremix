let debug = ref true

let set_debug x =
  debug := x

let is_debug () =
  !debug
