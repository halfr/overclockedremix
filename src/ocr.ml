(* ocr.ml *)
(* Author: Rémi Audebert *)

let usage =
  "Bertrand - OCR"

(* entry point *)
exception MissingArg of string

let main () =
  let input_path = ref ""
  and output_path = ref ""
  and use_denoise = ref false
  and use_gauss = ref false
  and use_sharpen = ref false
  and use_grayscale = ref false
  and use_contrast = ref false
  and use_unskew = ref false

  and horizontal_boxes = ref []
  and vertical_boxes = ref []

  in
  let speclist = [
    ("-i", Arg.Set_string input_path, "input image");
    ("-o", Arg.Set_string output_path, "output image");
    ("-denoise", Arg.Set use_denoise, "applay denoise filter (default: box blur)");
    ("-gauss", Arg.Set use_gauss, "use gaussian filter instead of box blur");
    ("-sharpen", Arg.Set use_sharpen, "apply sharpen filter");
    ("-gray", Arg.Set use_grayscale, "convert to grayscale");
    ("-contrast", Arg.Set use_contrast, "increase contrast");
    ("-unskew", Arg.Set use_unskew, "unskew image");
  ] in

  begin
    Arg.parse speclist (fun _ -> ()) usage;

    if !input_path = "" then raise (MissingArg "No input image");
    if !output_path = "" then raise (MissingArg "No output image");

    (* open image *)
    let surface = ref (Sdlloader.load_image !input_path) in begin
      (* display image after each filter *)
      Helpers.sdl_init ();
      let w, h = Helpers.surface_size !surface in
      let display = Sdlvideo.set_video_mode w h [`DOUBLEBUF] in begin
        Helpers.surface_show !surface display;
        Helpers.wait_key ();

        if !use_unskew then begin
          Printf.printf "Unskewing image...%!";
          surface := Filter_unskew.filter_unskew !surface;
          Helpers.surface_show !surface display;
          Printf.printf " done!\n%!";
          Helpers.wait_key ();
        end;

        if !use_denoise then begin
          if !use_gauss then begin
            Printf.printf "Gaussian blur filter...%!";
            surface := Filter_denoise_gaussian_blur.filter_denoise_gaussian_blur
                         !surface
          end
          else begin
            Printf.printf "Box blur filter...%!";
            surface := Filter_denoise_box_blur.filter_denoise_box_blur !surface;
          end;
          Helpers.surface_show !surface display;
          Printf.printf " done!\n%!";
          Helpers.wait_key ();
        end;

        if !use_contrast then begin
          Printf.printf "Contrast filter...%!";
          surface := Filter_contrast.filter_contrast !surface;
          Helpers.surface_show !surface display;
          Printf.printf " done!\n%!";
          Helpers.wait_key ()
        end;

        if !use_sharpen then begin
          Printf.printf "Sharpen filter...%!";
          surface := Filter_denoise_sharpen.filter_denoise_sharpen !surface;
          Helpers.surface_show !surface display;
          Printf.printf " done!\n%!";
          Helpers.wait_key ()
        end;

        if !use_grayscale then begin
          Printf.printf "Convert to grayscale...%!";
          Filter_grayscale.filter_grayscale !surface;
          Helpers.surface_show !surface display;
          Printf.printf " done!\n%!";
          Helpers.wait_key ();
        end;

        Printf.printf "Otsu threshold binarization...%!";
        surface := Filter_binarization_otsu.filter_binarization_otsu !surface;
        Helpers.surface_show !surface display;
        Printf.printf " done!\n%!";
        Helpers.wait_key ();
        end;

        Printf.printf "Horizontal paragraph detection...%!";
        horizontal_boxes := Analyse_box_detect.horizontal_box_detect !surface;
        vertical_boxes := Analyse_box_detect.vertical_box_detect !surface
                            !horizontal_boxes;
        Helpers.draw_boxes !surface Sdlvideo.red !horizontal_boxes;
        Helpers.surface_show !surface display;
        Printf.printf " done!\n%!";
        Helpers.wait_key ();
        Helpers.draw_boxes !surface Sdlvideo.green !vertical_boxes;
        Helpers.surface_show !surface display;
        Helpers.wait_key ();

    end;

  Sdlvideo.save_BMP !surface !output_path
  end

let _ =
  main ()
