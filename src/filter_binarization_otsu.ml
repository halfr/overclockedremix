(* filter_binarization_otsu.ml *)

exception Break
exception Continue

(* activate debug prints *)
let debug = false

(* Source:
 * http://www.labbookpages.co.uk/software/imgProc/otsuThreshold.html *)

let square x =
  x *. x

let grey_of_pixel = function
    (* because input image is in grayscale: r == g == b *)
  | (x, _, _) -> x

let histogram surface =
  let hist = Array.make 256 0 in
  let f surface (x, y) =
    let pixel = Sdlvideo.get_pixel_color surface x y in
    let grey_value = grey_of_pixel pixel in
      hist.(grey_value) <- hist.(grey_value) + 1
  in begin
    Helpers.surface_iter f surface;
    hist
  end

let print_histogram hist =
  for i = 0 to 255 do
    Printf.printf "%d:%d\n" i hist.(i)
  done

let otsu_threshold surface =
  let hist = histogram surface in
    begin
      if debug then print_histogram hist;

      let pixel_count =
        let w, h = Helpers.surface_size surface
        in w * h
      and sum = ref 0.
      and sum_background = ref 0.
      and weight_background = ref 0
      and weight_foreground = ref 0
      and variance_max = ref 0.
      and threshold = ref 0 in
      let accumulate surface (x, y) =
        sum := !sum +. (float (grey_of_pixel
                                 (Sdlvideo.get_pixel_color surface x y)))

      in
        begin
          Helpers.surface_iter accumulate surface;
          if debug then Printf.printf "Total pixel value: %f\n" !sum;

          try
            for t = 0 to 254 do
              try
                weight_background := !weight_background + hist.(t);
                if !weight_background = 0 then raise Continue;
                weight_foreground := pixel_count - !weight_background;
                if !weight_foreground = 0 then raise Break;

                sum_background := !sum_background +. (float (t * hist.(t)));

                let mean_background = !sum_background /. (float !weight_background)
                and mean_foreground = (!sum -. !sum_background) /. (float !weight_foreground)
                in
                let variance_between =
                  (float !weight_background) *. (float !weight_foreground) *.
                  (mean_background -. mean_foreground) ** 2.
                in
                  if variance_between > !variance_max then
                    begin
                      variance_max := variance_between;
                      threshold := t
                    end
              (* continue mechanism *)
              with Continue -> ()
            done
          with Break -> ();
        end;
        !threshold
    end

let filter_binarization_otsu surface =
  let threshold = otsu_threshold surface in
  let surface = Helpers.surface_copy surface in
  let pixel_threshold surface (x, y) =
    let grey_value = grey_of_pixel (Sdlvideo.get_pixel_color surface x y)
    in
      Sdlvideo.put_pixel_color surface x y
        (if grey_value > threshold then
           Sdlvideo.white
         else
           Sdlvideo.black)
  in
    begin
      Helpers.surface_iter pixel_threshold surface;
      surface
    end
