(* filter_contrast.ml *)

let filter_contrast surface =
  let contrast_kernel = [|
    [|  0.; -1.;  0. |];
    [| -1.;  5.; -1. |];
    [|  0.; -1.;  0. |];
  |] in
    Lib_convolution.convolve_value
      ~src:surface
      ~kernel:contrast_kernel
      ~divisor:2.0
      ~offset:0.0
