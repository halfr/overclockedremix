(* filter_denoise_sharpen.ml *)

let filter_denoise_sharpen src =
  let sharpen_kernel = [|
    [| -1.; -1.; -1. |];
    [| -1.;  9.; -1. |];
    [| -1.; -1.; -1. |];
  |] in
    Lib_convolution.convolve_value
      ~src:src
      ~kernel:sharpen_kernel
      ~divisor:0.5
      ~offset:0.0
