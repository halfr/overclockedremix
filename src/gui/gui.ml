(* gui.ml *)

let _ =
  Gdk.Rgb.init ();
  GtkBase.Widget.set_default_visual (Gdk.Rgb.get_visual ());
  GtkBase.Widget.set_default_colormap (Gdk.Rgb.get_cmap ())

let win = GWindow.window ()
let box = GPack.vbox ~packing:win#add ()

let menubar = GMenu.menu_bar ~packing:box#pack ()
let menu_factory = new GMenu.factory menubar

let cb_foo () = print_endline "oui oui"

let image_menu = menu_factory#add_submenu "Image"
and preprocess_menu = menu_factory#add_submenu "Preprocessing"
and bin_menu = menu_factory#add_submenu "Binarization"
and char_menu = menu_factory#add_submenu "Character"
and text_menu = menu_factory#add_submenu "Text"
and help_menu = menu_factory#add_submenu "Help"

let current_image_path = ref (Some "")
let current_image_rev = ref 0

let next_image_path () =
  incr current_image_rev;
  match !current_image_path with
    | None -> string_of_int !current_image_rev
    | Some path ->
        Printf.sprintf "%s.%d" path !current_image_rev

(* Add image to gui *)

let add_image_textview (buffer:GText.buffer) path =
  let pixbuf = GdkPixbuf.from_file path in
    let iter = buffer#get_iter_at_char 0 in
      buffer#insert_pixbuf ~iter ~pixbuf

let cb_new_image buffer = function
  | None -> ()
  | Some filename -> add_image_textview buffer filename

(* Preprocessing cb helpers *)

(* image_from:path -> image_to:path -> unit *)
let meta_filter bin_name = fun image_from image_to ->
    ignore (
      Sys.command (Printf.sprintf
        "../bin/%s.native -i \"%s\" -o \"%s\""
        bin_name
        image_from
        image_to)
      )

let pp_unskew = meta_filter "tool_filter_unskew"
let pp_contrast = meta_filter "tool_filter_contrast"
let pp_boxblur = meta_filter "tool_filter_denoise_box_blur"
let pp_gaussblur = meta_filter "tool_filter_denoise_gaussian_blur"
let pp_sharpen = meta_filter "tool_filter_denoise_sharpen"
let pp_grayscale = meta_filter "tool_filter_grayscale"
let bin_otsu = meta_filter "tool_filter_binarization_otsu"
let charbox = meta_filter "tool_analyse_box_detect"

(* Main gui setup *)

let _ =
  let textview = GText.view () in
  let textbuffer = textview#buffer in
  let sw = GBin.scrolled_window ~packing:box#add
  ~hpolicy:`AUTOMATIC ~vpolicy:`AUTOMATIC () in
  sw#add textview#coerce;

  let image_menu_factory = new GMenu.factory image_menu in
  ignore (image_menu_factory#add_item "Open new image..."
    ~callback:(fun () ->
      Imagechooser.ask_for_file win current_image_path;
      cb_new_image textbuffer !current_image_path));
  ignore (image_menu_factory#add_item "Save current as..." ~callback:cb_foo);
  ignore (image_menu_factory#add_separator ());
  ignore (image_menu_factory#add_item "Quit" ~callback:GMain.Main.quit);

  let image_processing_cb f = begin function () ->
    match !current_image_path with
      | None -> ()
      | Some image_from -> begin
        let image_to = next_image_path () in
        current_image_path := Some image_to;
        f image_from image_to;
        add_image_textview textbuffer image_to
      end
  end in

  let preprocess_menu_factory = new GMenu.factory preprocess_menu in
  ignore (preprocess_menu_factory#add_item "Unskew"
    ~callback:(image_processing_cb pp_unskew));
  ignore (preprocess_menu_factory#add_item "Increase contrast"
    ~callback:(image_processing_cb pp_contrast));
  ignore (preprocess_menu_factory#add_item "Box blur"
    ~callback:(image_processing_cb pp_boxblur));
  ignore (preprocess_menu_factory#add_item "Gaussian blur"
    ~callback:(image_processing_cb pp_gaussblur));
  ignore (preprocess_menu_factory#add_item "Sharpen"
    ~callback:(image_processing_cb pp_sharpen));
  ignore (preprocess_menu_factory#add_item "Grayscale"
    ~callback:(image_processing_cb pp_grayscale));

  let bin_menu_factory = new GMenu.factory bin_menu in
    ignore (bin_menu_factory#add_item "Otsu binarization"
    ~callback:(image_processing_cb bin_otsu));

  let char_menu_factory = new GMenu.factory char_menu in
  ignore (char_menu_factory#add_item "Box detect chararecters"
    ~callback:(image_processing_cb charbox));

  let help_menu_factory = new GMenu.factory help_menu in
  help_menu_factory#add_item "About"
    ~callback:
    begin fun () ->
      let dialog =
          GWindow.about_dialog
          ~name:"Bertrand"
          ~authors:["CAPTCHA Perché" ;
          "Rémi Audebert";
          ]
          ~copyright:"Copyright: bbqware"
          ~license:"Open"
          ~website:"https://github.com/Nervous/OCR/"
          ~website_label:"Site web"
          ~version:"42"
          () in begin
              ignore (dialog#connect#response ~callback:(fun _ -> dialog#destroy
              ()));
              ignore (dialog#show ())
          end
    end

let _ =
  ignore (win#connect#destroy ~callback:GMain.Main.quit);
  win#show();
  GMain.Main.main ()

(* vim:set sw=2: *)
