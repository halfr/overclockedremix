(* tool_filter_grayscale.ml *)

let _ =
  Helpers.main_filter
    "Convert color image to grayscale using luminance function"
    Filter_grayscale.filter_grayscale
