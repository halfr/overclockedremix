(* analyse_box_detect.ml *)

let horizontal_box_detect surface =
  let width, height = Helpers.surface_size surface
  and boxes = ref []
  and inside_box = ref false
  and box_start = ref 0 in
  let threshold = width / 100 in
    begin
      for y = 0 to height - 1 do

        (* count black pixels on a line *)
        let black_pixel_count = ref 0 in
          begin
            for x = 0 to width - 1 do
              if Helpers.is_black surface x y then
                incr black_pixel_count
            done;

            if !black_pixel_count > threshold then
              begin
                if not !inside_box then
                  begin
                    inside_box := true;
                    box_start := y
                  end
              end
            else if !inside_box then
              begin
                boxes := (0, !box_start, width, y) :: !boxes;
                inside_box := false
              end
          end
      done;

      !boxes
    end

let rec vertical_box_detect surface = function
  | (x_up_left, y_up_left, x_bot_right, y_bot_right) :: boxes ->
      begin
        let vertical_boxes = ref []
        and inside_box = ref false
        and box_start = ref 0
        and threshold = (y_bot_right - y_up_left) / 5 in
(*         and threshold = 3 in *)

          for x = x_up_left to x_bot_right do


            let black_pixel_count = ref [] in
              begin
                for y = y_up_left to y_bot_right do
                  if Helpers.is_black surface x y then
                    black_pixel_count := y :: !black_pixel_count
                done;

                if List.length !black_pixel_count > threshold then
                  begin
                    if not !inside_box then
                      begin
                        inside_box := true;
                        box_start := x
                      end
                  end
                else if !inside_box && List.length !black_pixel_count < 1 then
                  begin
                    vertical_boxes := (!box_start, y_up_left, x - 1, y_bot_right)
                                        :: !vertical_boxes;
                    inside_box := false;
                  end
              end
          done;

        !vertical_boxes @ vertical_box_detect surface boxes
      end
  | [] -> []
