(* tool_filter_binarization_ostu.ml *)

let _ =
  Helpers.main_filter_copy
    "Otsu binarization"
    Filter_binarization_otsu.filter_binarization_otsu;
