(* helpers.ml *)

(* init sdl *)
let sdl_init () =
  Sdl.init [`EVERYTHING];
  Sdlevent.enable_events Sdlevent.all_events_mask


(* Coord. format: *)
(* x-----> *)
(* y       *)
(* |       *)
(* |       *)
(* |       *)
(* v       *)

(* surface_size: return (width, height) of `surface' *)
let surface_size surface =
  ((Sdlvideo.surface_info surface).Sdlvideo.w,
   (Sdlvideo.surface_info surface).Sdlvideo.h)

(* surface_iter: iterate over `surface' calling `f' on each pixel *)
let surface_iter f surface =
  let width, height = surface_size surface in
    for x = 0 to width - 1 do
      for y = 0 to height - 1 do
        begin
          f surface (x, y)
        end
      done
    done

let surface_copy surface =
  let w, h = surface_size surface in
  let new_surface = Sdlvideo.create_RGB_surface_format
                      surface
                      [ `SWSURFACE ; `SRCALPHA ; `SRCCOLORKEY ]
                      w h in
    Sdlvideo.blit_surface surface new_surface ();
    new_surface

let surface_show img dst =
  let d = Sdlvideo.display_format img in
    Sdlvideo.blit_surface d dst ();
    Sdlvideo.flip dst

let rec wait_key () =
  match Sdlevent.wait_event () with
    | Sdlevent.KEYDOWN _ -> ()
    | _ -> wait_key ()

(* use on monochrome images *)
let is_black surface x y =
  let x, _, _ = Sdlvideo.get_pixel_color surface x y in
    x = 0 (* 0 = balck *)

let draw_rectangle surface color x_up_left y_up_left x_bot_right y_bot_right =
  for x = x_up_left to x_bot_right do
    for y = y_up_left to y_bot_right do
      if not (is_black surface x y) then
        Sdlvideo.put_pixel_color surface x y color
    done
  done

let rec draw_boxes surface color = function
  | (x_up_left, y_up_left, x_bot_right, y_bot_right) :: boxes ->
      begin
        draw_rectangle surface color
          x_up_left
          y_up_left
          x_bot_right
          y_bot_right;
        draw_boxes surface color boxes
      end
  | [] -> ()


(* filter tool main *)
exception MissingArg of string

(* main for filters that takes a surface and return a surface *)
let main_filter_copy usage f =
  let input_path = ref ""
  and output_path = ref ""
  and debug = ref false in
  let speclist = [
    ("-i", Arg.Set_string input_path, "input image");
    ("-o", Arg.Set_string output_path, "output image");
    ("-d", Arg.Set debug, "display sdl before/after");
  ]
  in begin
    Arg.parse speclist (fun _ -> ()) usage;
    if !input_path = "" then raise (MissingArg "No input image");
    if !output_path = "" then raise (MissingArg "No output image");
    let surface = Sdlloader.load_image !input_path in
      if !debug then begin
          sdl_init ();
          let w, h = surface_size surface in
          let display = Sdlvideo.set_video_mode w h [`DOUBLEBUF] in begin
            surface_show surface display;
            if Debug.is_debug () then Printf.printf "Press any key to start.\n%!";
            wait_key ();
            if Debug.is_debug () then Printf.printf "Working... %!";
            let surface = f surface in
              surface_show surface display;
            if Debug.is_debug () then Printf.printf " done!\n%!";
            wait_key ()
          end
      end
      else begin
        let surface = f surface in
          Sdlvideo.save_BMP surface !output_path
      end
  end

(* main for filters that does not copies it's input surface
* aka. with type val f : Sdlvideo.surface -> unit *)
let main_filter usage f =
  let f_copy surface =
    begin
      f surface;
      surface
    end
  in main_filter_copy usage f_copy

let main_tool usage =
  let input_path = ref "" in
  let speclist = [
    ("-i", Arg.Set_string input_path, "input image");
  ]
  in begin
    Arg.parse speclist (fun _ -> ()) usage;
    if !input_path = "" then raise (MissingArg "No input image");
    let surface = Sdlloader.load_image !input_path in
    let w, h = surface_size surface in
    let display = Sdlvideo.set_video_mode w h [`DOUBLEBUF] in
      (surface, display)
  end
