(* tool_filter_denoise_box_blur.ml *)

let _ =
  Helpers.main_filter_copy
    "Denoise using box blue"
    Filter_denoise_box_blur.filter_denoise_box_blur
