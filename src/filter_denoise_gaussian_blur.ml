(* filter_denoise_gaussian_blur.ml.ml *)

let filter_denoise_gaussian_blur surface =
  let gaussian_kernel = [|
    [|  1.;  2.;  1. |];
    [|  2.;  4.;  2. |];
    [|  1.;  2.;  1. |];
  |] in
    Lib_convolution.convolve_value
      ~src:surface
      ~kernel:gaussian_kernel
      ~divisor:16.0
      ~offset:0.0
