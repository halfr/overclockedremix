(* tool_analyse_box_detect.ml *)

let box_detect surface =
  let hboxes = Analyse_box_detect.horizontal_box_detect surface in
    let vboxes = Analyse_box_detect.vertical_box_detect surface hboxes in
        Helpers.draw_boxes surface Sdlvideo.red vboxes

let _ =
  Helpers.main_filter "Box detection" box_detect
