(* tool_filter_denoise_gaussian_blur.ml *)

let _ =
  Helpers.main_filter_copy
    "Denoise using gaussian blur"
    Filter_denoise_gaussian_blur.filter_denoise_gaussian_blur
