#!/bin/zsh

BIN=./main.native

emulate -L zsh
#set -e

autoload -U colors
colors

checking()
{
    if [ $# = 0 ]; then
        echo "$0 message"
        return 1
    fi
    echo -ne "${bg[black]}${fg_bold[white]}$*${reset_color} ... "
}

ok()
{
    echo -e "${fg_bold[green]}OK${reset_color}"
}

fail()
{
    echo -e "${fg_bold[red]}FAIL${reset_color}"
}

check_run()
{
    ${BIN}
}

check()
{
    checking Interspace connection
    ok
    checking Grayscale conversion
    ok
}

check
